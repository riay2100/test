![alt text](https://miun.imagevault.media/publishedmedia/2uodxqxz9ui7iq5xu0xr/Miun_logotyp_CMYK.svg "Logo")


# Programvaruteknik


**H�sten 2016 tar vi �ter in nya studenter till v�rt popul�ra program Programvaruteknik vid Mittuniversitetet i �stersund. Nytt �r att du kan l�sa programmet p� Campus i �stersund eller p� distans! Fokus i programmet �r att kunna bygga olika typer av applikationer f�r Webben, din dator eller f�r din smartphone. Mobilitet, distansoberoende, webbtj�nster och cloud computing �kar i betydelse och d�r �r vi med! Genom att l�sa programmet skaffar du dig en gedigen universitetsutbildning som ger dig verktygen att vara med att forma den digitala framtiden.**

Utbildningen erbjuder dig en spr�ngbr�da till ett arbete inom IT-branschen. Med modern teknik och metodik l�r du dig design och implementering av mjukvara f�r system som kan best� av s�v�l servrar, persondatorer/laptops som mobiler och plattor. Programmet har utvecklats i samarbete med IT-f�retag. F�r att l�sa programmet p� distans beh�ver du en bra internetuppkoppling, ett headset och en webbkamera f�r att kunna kommunicera M�tena �r allts� virtuella och du beh�ver inte f�rflytta dig till oss i �stersund.


# Sammankomster


Utbildningen erbjuder dig en spr�ngbr�da till ett arbete inom IT-branschen. Med modern teknik och metodik l�r du dig design och implementering av mjukvara f�r system som kan best� av s�v�l servrar, persondatorer/laptops som mobiler och plattor. Programmet har utvecklats i samarbete med IT-f�retagen i regionen. F�r att l�sa programmet p� distans beh�ver du en bra internetuppkoppling, ett headset och en webbkamera f�r att kunna kommunicera. M�tena �r allts� virtuella och du beh�ver inte f�rflytta dig till oss i �stersund.
L�ser du p� campus har du naturligtvis tillg�ng till allt distansmaterial ut�ver alla aktiviteterna p� plats i v�r egen labbsal. Nytt f�r i �r �r helt nya lokaler p� f�rsta v�ningen med eget klassrum/labbsal. Vi delar pentry, soffa och studieh�rna med Informatik-g�nget. De �r ju helt underl�gsna oss i programmering men det f�r v�l g� �nd�:)


Det f�rsta �ret inneh�ller n�dv�ndiga grundkurser som ska leda fram till en gedigen kompetens inom omr�det objektorienterad programmering. Det andra �ret �gnas �t olika aspekter av utveckling f�r webben med kopplingar till databaser, �verbryggning fr�n C++ till Java samt till�mpningar inom omr�det mobila enheter. Under utbildningen f�r du �ven arbeta i grupper med ett gemensamt ansvar f�r olika delar i kedjan fr�n id� till f�rdig produkt.

### Examen efter genomf�rda studier


Efter tre �r kan du ta ut en kandidatexamen med datateknik som huvud�mne och med inriktning mot programvaruteknik. Du kan alternativt ta ut en h�gskoleexamen med inriktning mot datateknik efter tv� �r om du v�ljer att inte l�sa det tredje �ret.


### Utbildningsupl�gg


Under utbildningen kommer du mest att l�sa kurser inom �mnesomr�det datateknik. Ditt virtuella klassrum i de olika kurserna kommer att vara den webbaserade utbildningsplattformen Moodle. D�r hittar du allt kursmaterial och d�r kommunicerar du med l�rare och dina medstudenter p� kurserna. F�r det mesta g�rs ocks� prov och redovisningar via webben.

V�ra kurser inneh�ller, f�rutom teori, en stor m�ngd praktiska uppgifter som ska utf�ras och redovisas. Ofta handlar det om programmeringsuppgifter som du ska l�sa sj�lv eller mindre projekt som du kan samarbeta med andra studenter om. M�nga av kurserna bygger vidare p� tidigare kurser och p� s� vis f�rdjupas kunskaperna samtidigt som du till�mpar kunskaper fr�n tidigare kurser. Eftersom allt kursmaterial kommer att finnas tillg�ngligt i Moodle kommer du inte att vara bunden att passa tider f�r f�rel�sningar mm. D�remot kommer redovisningar och prov att vara bundna till vissa tider.


### Kurser


**�r 1**

    - Datavetenskaplig introkurs (LP1)
    - Operativsystem introduktion med till�mpningar i Linux (LP1)
    - Introduktion till programmering i C++ (LP2)
    - Datakommunikation och n�tverk med till�mpningar i Linux (LP2)
    - Objektbaserad programmering i C++ (LP3)
    - Diskret matematik f�r programmerare (LP3+4)
    - Databaser, modellering och implementering (LP3+4)
    - Objektorienterad programmering i C++ (LP4)
    - Valbar kurs: Avancerad felprogrammering
    


**�r 2**

    - Metoder och verktyg i mjukvaruprojekt (LP1)
    - XML (LP1)
    - Java f�r C++ programmerare (LP2)
    - Webbprogrammering med HTML5, CSS3 och JavaScript (LP2)
    - Designm�nster med C++ (LP3)
    - Webbprogrammering med PHP och PostgreSQL (LP3)
    - Applikationsutveckling f�r Android (LP4)
    - S�kerhet i mjukvara (LP4)
    - Valbar kurs: Vattnets betydelse f�r sj�farten


**�r 3** 

    - Valbar kurs: Murphy's law i teori och praktik
    - Presentation av ny teknik (LP1+2)
    - Java Enterprise-utveckling med EE-standarden (LP1+2)
    - Systemprogrammering i UNIX/Linux (LP1)
    - Programmering med samtidighet och parallellism (LP2)
    - Artificiell Intelligens f�r agenter (LP3)
    - Till�mpad datateknik, mjukvaruprojekt (LP3)
    - Sj�vst�ndigt arbete (LP4)

